//
// Utils header file
//
// Questa libreria contiene delle utili funzioni al fine di
// velocizzare/semplificare la programmazione
//
// Creato da Cristian "J4Rr3x" Cosenza
// Contributori:
// - N/A
//
// Come contribuire?
// Usare la notazione quando si dichiarano le variabili,
// rispettare l'ordine ed inserire i commenti sulle funzioni
// compreso il vostro nome
//

//
// Funzioni:
// 	void Swap(int &iVariable1, int &iVariable2)
//	int random(int iMin, iMax)
// Macro:
//	GetArraySize(Array)

#if !defined(_GLIBCXX_IOSTREAM)
	#include <iostream>
#endif // !defined(_GLIBCXX_IOSTREAM)
#if !defined(_GLIBCXX_CSTDLIB)
	#include <cstdlib>
#endif // !defined(_GLIBCXX_CSTDLIB)

using namespace std;

//
// void Swap(int &iVariable1, int &iVariable2)
// Esegue lo swap tra due variabili
// Autore: Cristian "J4Rr3x" Cosenza
//
void Swap(int &iVariable1, int &iVariable2) {
	
	int
		iTemp;
		
	iTemp = iVariable1;
	iVariable1 = iVariable2;
	iVariable2 = iTemp;
	
}

//
// int random(int iMin, int iMax)
// Restituisce un numero random compreso tra iMin e iMax
// Autore: Cristian "J4Rr3x" Cosenza
//
int random(int iMin, int iMax) {
	
	return (rand() % (iMax - iMin + 1) + iMin);
	
}

//
// GetArraySize(Array);
// Restituisce la grandezza dell'array specificata
// Autore: Cristian "J4Rr3x" Cosenza
//
#define GetArraySize(Array)	(sizeof(Array) / sizeof(*Array))
