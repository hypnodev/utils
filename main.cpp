//
// Main test program file
//
// Main.cpp per il programma di esempio per le funzione di utils.h
//
// Creato da Cristian "J4Rr3x" Cosenza
// Contributori
// - N/A
//

#include "utils.h"

//
// int main()
// Funziona principale del programma
//
int main() {
	
	int
		iArray[2]
	;
		
	cout << "Benvenuto nel programma di esempio per Utils.h" << endl;
	cout << "Creato da: Cristian \"J4Rr3x\" Cosenza" << endl << endl;
		
	iArray[0] = random(0, 100);		
	iArray[1] = random(0, 100);	
	
	cout << "Grandezza dell'array iArray: " << GetArraySize(iArray) << endl;

	cout << "Valore degli elementi di iArray: " << endl;
	for(int i = 0; i < GetArraySize(iArray); i++) {
		cout << i << ". " << iArray[i] << endl;
	}
	
	cout << endl;

	Swap(iArray[0], iArray[1]);	
	
	cout << "Valori dopo lo swap: " << endl;
	for(int i = 0; i < GetArraySize(iArray); i++) {
		cout << "iArray["<< i << "] = " << iArray[i] << endl;
	}
	
	system("pause");
	return 0;
	
}
